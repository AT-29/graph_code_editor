import cytoscape from "./lib/tree.js";

import {Code} from './editor/main.js';

import {
  removeNode,
  removeEdge,
  connectNodes,
  clickNodes,
  clickEdges,
  inspectSelectionIndex,
  showNodeText,
  addNodeText,
  addNode,
  clearSelection,
  showToolTip,
  displayStats,
} from "./graph/main.js";
import {
  test,
  sendJSONData,
  pruneTree,
  loadFile,
  clearTree,
  getJSONData,
  compile,
  clearFileOptions,
  createFileSelect,
  loadSelectedFile,
  getScroll,
} from "./graph/utils.js";
import { memo, elements } from "./graph/common.js";

export const cy = cytoscape({
  elements: [],
  container: document.getElementById("tree"),
  style: [
    { selector: "edge", style: { width: 2, "curve-style": "straight","line-color":"#99b2ff" } },
    {
      selector: "edge[arrow]",
      style: {
        "target-arrow-fill": "filled ",
        "target-arrow-shape": "diamond",
        "target-arrow-color":"#99b2ff"
      },
    },
    {
      selector: "node",
      style: {
        "border-style":"solid",
        "border-color":"#99b2ff",
        "border-width":"2",
        "background-color": "transparent",
        "shape ": "diamond",
        
      },
    },
    {
      selector: "node:active",
      style: {
        "background-color": "#99b2ff",
      },
    },
  ],
  layout: { name: "breadthfirst" },
  fit: true,
  padding: 30, // fit padding
  // initial viewport state:
  zoom: 1,
  pan: { x: 0, y: 0 },

  // interaction options:
  minZoom: 1e-50,
  maxZoom: 1e50,
  zoomingEnabled: true,
  userZoomingEnabled: true,
  panningEnabled: true,
  userPanningEnabled: true,
  boxSelectionEnabled: true,
  selectionType: "single",
  touchTapThreshold: 8,
  desktopTapThreshold: 4,
  autolock: false,
  autoungrabify: false,
  autounselectify: false,

  // rendering options:
  headless: false,
  styleEnabled: true,
  hideEdgesOnViewport: false,
  textureOnViewport: false,
  motionBlur: false,

  wheelSensitivity: 0.3,
  pixelRatio: "auto",
});

cy.ready(() => {
  document.addEventListener("mousemove", (e) => {
    const pan = cy.pan();
    const zoom = cy.zoom();
    const scrollOffset = getScroll();
    memo.mousePosition.x = (e.clientX - pan.x + scrollOffset.x) / zoom;
    memo.mousePosition.y = (e.clientY - pan.y + scrollOffset.y) / zoom;
  });
  document.addEventListener("keydown", (e) => {
    if (e.isComposing || e.key.toLowerCase() === "t") {
      test();
    }
    if (e.isComposing || e.key.toLowerCase() === "c") {
      if(memo.focus===elements.treeContainer){
        connectNodes();
      }
    }
    if(e.isComposing || e.key.toLowerCase() === "q" && e.ctrlKey){
      if(memo.lastSelection.id){
      memo.selectedPairs.length = 0;
      addNodeText(memo.lastSelection.id, Code.editor.getValue());
      Code.setTabs(compile())
        const code = Code.editor.getValue()
        addNodeText(memo.lastSelection.id, code);
        Code.tabs.set(memo.lastSelection.id,`const ${memo.lastSelection.id}=(input)=>{
          let output=input;
          ${code};
          return output;
        };`)
        Code.runCode();
      }
    }
 
    if ( e.isComposing || e.key.toLowerCase() === "n"  ) {
      memo.lastSelection = { id: null };
      inspectSelectionIndex({ type: "not selected", id: "none" });
      clearSelection();
      if(memo.focus===elements.treeContainer){
        addNode(
          memo.nodeIndex,
          memo.mousePosition.x,
          memo.mousePosition.y,
         `output = input`
        );
        Code.tabs.set( "n"+memo.nodeIndex,`const n${memo.nodeIndex}=(input)=>{let output=input;output=input;return output};`);

      }
      
      
    }
    // if(e.isComposing || e.key.toLowerCase() === "q" && e.shiftKey && e.ctrlKey){
    //   if(memo.lastSelection.id){
    //     const code = Code.editor.getValue()
    //     addNodeText(memo.lastSelection.id, code);
    //     Code.tabs.set(memo.lastSelection.id,`const ${memo.lastSelection.id}=(input)=>{${code}};`)
    //     Code.runCode();
    //   }
    // }


    if (e.isComposing || e.key === "Escape") {
      clearSelection();
      inspectSelectionIndex({ type: "not selected", id: "none" });
    }
    if (e.isComposing || e.key === "Delete") {
      if (memo.lastSelection.type === "node") {
        removeNode(memo.lastSelection.id);
      } else if (memo.lastSelection.type === "edge") {
        removeEdge(memo.lastSelection.id);
      }
      clearSelection();
      inspectSelectionIndex({ type: "not selected", id: "none" });
    }
  });
  // cy.on("drag", "node", (e) => {
  //   inspectSelectionIndex({ type: "not selected", id: e.target.id() });
  //   memo.selectedPairs.length = 0;
  // });
  cy.on("dragfree", "node", (e) => {
    // inspectSelectionIndex({ type: "not selected", id: "none" });
    memo.selectedPairs.length = 0;
    clickNodes(e);
  });

  cy.on("click", "node", (e) => {
    memo.focus=elements.treeContainer
    showNodeText(e.target.id());
    //e.originalEvent.ctrlKeyconnectNodes
    clickNodes(e);
  
  });
  cy.on("mouseout", "node", (e) => {
    if (e) {
      elements.infoGuide.style.display = "none";
    }
    // clickNodes(e,e.originalEvent.ctrlKey);
  });
  cy.on("mouseover", "node", (e) => {
    if (e && e.originalEvent.altKey) {
      showToolTip(displayStats(e.target.data()));
    }
    // clickNodes(e,e.originalEvent.ctrlKey);
  });
  cy.on("click", "edge", (e) => {
    clickEdges(e);
    const data = e.target.data();
    inspectSelectionIndex(
      memo.lastSelection,
      "[ " + data.source + " -> " + data.target + " ]"
    );
  });


  elements.compileButt.addEventListener("click", () => {
    if(memo.lastSelection.id){
      memo.selectedPairs.length = 0;
      addNodeText(memo.lastSelection.id, Code.editor.getValue());
      Code.setTabs(compile())
        const code = Code.editor.getValue()
        addNodeText(memo.lastSelection.id, code);
        Code.tabs.set(memo.lastSelection.id,`const ${memo.lastSelection.id}=(input)=>{let output=input;${code};return output;};`)
        Code.runCode();
        elements.output.value = Code.result;

      }
  });
  elements.titleInput.addEventListener("change", (e) => {
    memo.data.title = e.target.value;
  });

  elements.toJSONButt.addEventListener("click", () => {
    sendJSONData();
  });
  elements.clearButt.addEventListener("click", () => {
    clearTree();
  });

  elements.fromJSONButt.addEventListener("click", () => {
    elements.formData.click();
  });

  elements.levelInput.addEventListener("change", (e) => {
    memo.data.lvl = +e.target.value;
  });
  elements.fileSelector.addEventListener("dblclick", (e) => {
    clearFileOptions();
    createFileSelect();
  });
  elements.fileSelector.addEventListener("click", (e) => {
    if(e.target.constructor.name === "HTMLOptionElement"){
      loadSelectedFile(e.target.value);
    [...document.getElementsByClassName("options")].map((x)=>{
      x.style.backgroundColor="transparent";
      x.style.color = "white";
    })
    e.target.style.backgroundColor = '#99b2ff';
    e.target.style.color = "black";
    }
    });
  elements.formData.addEventListener("click", (e) => {
    getJSONData();
    setTimeout(() => {
      loadFile();
    }, 1000);
  });
});


Code.element.addEventListener("click",(e)=>{
 memo.focus = e.target;
})

elements.treeContainer.addEventListener("click",(e)=>{
 memo.focus = elements.treeContainer
})
