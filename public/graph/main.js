import { cy } from "../events.js";

import { memo, elements } from "./common.js";
import {Code} from '../editor/main.js'
const setIndex = (v) => {
  memo.nodeIndex = +v;
  memo.edgeIndex += memo.nodeIndex;
};
const incIndex = (v = 1) => {
  memo.nodeIndex += v;
  // elements.indexInput.value = memo.nodeIndex;
};
const addNode = (index, x = 0, y = 0,text) => {
  if (!cy.nodes(`#${"n" + index}`).length) {
    const node = cy.add({
      group: "nodes",
      data: {
        id: "n" + index,
        index,
        name: "",
        text: text,
        type: "",
      },
    });
    node.position({ x, y });
    incIndex();
    return node;
  } else {
    const nodeLen = cy.nodes().length;
    setIndex(nodeLen - 1);
    addNode(nodeLen, x, y);
  }
};

const addEdge = (index, prevId, nextId) => {
  const edge = cy.add({
    group: "edges",
    data: {
      id: `e${index}`,
      source: `${prevId}`,
      target: `${nextId}`,
      arrow: "diamond",
    },
  });
  memo.edgeIndex += 1;
  return edge;
};

const addNodeText = (id, text) => {
  if (memo.lastSelection.type === "node") cy.nodes(`#${id}`).data().text = text;
  // if (memo.lastSelection.type === "edge") cy.edges(`#${id}`).data().text = text;
};


const showNodeText = (id) => {
  const data = cy.nodes(`#${id}`).data();
  Code.editor.setValue(data.text)
  // elements.textArea.value = data.text;
};

const inspectSelectionIndex = (selection, opt = "") =>
  (elements.selectedIndex.innerHTML = `${selection.id || "none"} : ${
    selection.type || "not selected"
  } ${opt}`);

const clickEdges = (e) => {
  memo.lastSelection.type = "edge";
  memo.lastSelection.id = e.target.id();
  memo.selectedPairs.length = 0;
};

const connectNodes = () => {
  const couple = memo.selectedPairs;
  if (memo.selectedPairs.length > 1) {
    if (
      !cy
        .edges()
        .find(
          (x) =>
            (x.data().source === couple[1] && x.data().target === couple[0]) ||
            (x.data().source === couple[0] && x.data().target === couple[1])
        )
    ) {
      addEdge(memo.edgeIndex, ...couple);
      memo.selectedPairs.length = 0;
      inspectSelectionIndex(
        memo.lastSelection,
        couple[1]
          ? "[ " + memo.selectedPairs.join(" -> ") + " ]"
          : "[ " + memo.lastSelection.id + " -> ? ]"
      );
      memo.selectedPairs.push(memo.lastSelection.id);
    } else {
      // showToolTip("");
      memo.selectedPairs.length = 0;
    }
  }
};

const clickNodes = (e) => {
  memo.lastSelection.type = "node";
  memo.lastSelection.id = e.target.id();
  memo.selectedPairs.push(memo.lastSelection.id);
  const couple = memo.selectedPairs;
  
  if (memo.selectedPairs.length > 2) {
   memo.selectedPairs[0] = memo.selectedPairs[1]
   memo.selectedPairs.length = 1;
   memo.selectedPairs.push(memo.lastSelection.id);
  }
  inspectSelectionIndex(
    memo.lastSelection,
    couple[1]
      ? "[ " + memo.selectedPairs.join(" -> ") + " ]"
      : "[ " + memo.lastSelection.id + " -> ? ]"
  );
};

const removeNode = (id) => {
  cy.nodes(`#${id}, edge[source = "${id}`).remove();
};
const removeEdge = (id) => {
  const edge = cy.edges(`#${id}`);
  const edgeData = edge.data();

  edge.remove();
  cy.nodes(`#${id}, edge[source = "${id}`).remove();
};

const clearSelection = () => {
  memo.selectedPairs.length = 0;
  memo.lastSelection = { id: null };
};

const objToString = (obj) =>
  Object.entries(obj).map(
    (item, index) =>
      `${item[0]}:${
        index > 1 && item[1].length > 10
          ? item[1].toString().substr(0, 10) + "..."
          : item[1]
      }`
  );

const displayStats = (obj) =>
  objToString(obj).join("\n");

const showToolTip = (msg) => {
  elements.tooltip.innerHTML = msg;
  elements.infoGuide.style.display = "block";
  // elements.mouseGuide.style.top = memo.mousePosition.y + "px";
  // elements.mouseGuide.style.left = memo.mousePosition.x + "px";
};
const seedGraph = (nodes=memo.elements.nodes,edges=memo.elements.edges) =>{
  cy.add([...nodes, ...edges]);
}
export {
  seedGraph,
  connectNodes,
  clickEdges,
  clearSelection,
  removeNode,
  removeEdge,
  clickNodes,
  inspectSelectionIndex,
  showNodeText,
  setIndex,
  addNodeText,
  incIndex,
  addEdge,
  addNode,
  showToolTip,
  displayStats,
};
