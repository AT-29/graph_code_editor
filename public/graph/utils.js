import { cy } from "../events.js";
import { inspectSelectionIndex, clearSelection, incIndex, removeNode,seedGraph } from "./main.js";
import { memo, elements, VERSION } from "./common.js";
import { API, PORT } from "../../config.js";
const clearFileOptions = () =>{
  const opt = elements.fileSelector.querySelector("optgroup");
   elements.fileSelector.innerHTML = ''
   elements.fileSelector.appendChild(opt)
 }
 const loadSelectedFile = (filename) =>{
  fetch(`${API}${PORT}/public/data/graph/${filename}`)
  .then(res=>res.json())
  .then((res)=>{
      clearTree();
      const parsedJSON = res;
      memo.elements = offsetElementsIndexes(parsedJSON.elements);
      memo.data = parsedJSON.data;
      elements.titleInput.value = parsedJSON.data.title;
      seedGraph()
  })
}
const createFileSelect = () =>{

  fetch(`${API}${PORT}/ls`).then(res=>res.json()).then((data)=>{
    data.items.map((file)=>{
    const opt = document.createElement('option');
    opt.className = 'options';
    opt.value = file
    opt.innerHTML = file
    elements.fileSelector.appendChild(opt)
    })
  });


}
const clearTree = () => {
  memo.nodeIndex = 0;
  memo.edgeIndex = 0;
  cy.nodes().remove();
  cy.edges().remove();
  // elements.indexInput.value = 0;
};
const pruneTree = () => {
  memo.nodeIndex = 0;
  let max = 0,
    min = 0;
  cy.nodes().map((n) => {
    const data = n.data();
    if (!data.name.trim()) removeNode(data.id);
    min = Math.min(data.index, min);
    max = Math.max(data.index, max);
  });
  memo.elements.nodes = cy.nodes().map((x) => x.data());
  memo.elements.edges = cy.edges().map((x) => x.data());
  memo.edgeIndex = Math.max(memo.edgeIndex,cy.edges().length)
  incIndex(max + 1);
};

const compile = () => {
  // const root = cy.nodes().roots().data();
    //pathTo(root.id, memo.lastSelection.id || root.id);
  const current =  cy.nodes(`#${memo.lastSelection.id}`);
  const path = current.predecessors();
  const out =  path.reduceRight((a,x)=>{
    const current = x.data();
    if(current.id[0]==="n") a.push(current)
    return a;
  },[])
  // console.log(current.incomers().map(x=>x.data()))
  out.push(current.data());
  return {data:out,path}
};

const pathTo = (start,end)=>{
  const dijkstra = cy.elements().dijkstra('#'+start);
  const pathTo = dijkstra.pathTo( cy.$('#'+end) );
  return {data:pathTo.map(x=>x.data()),path:pathTo}
}
const getJSONData = () => {
  const json = elements.formData.files[0];
  const formData = new FormData();
  formData.append("photo", json);
};

const upload = async (data) => {
  return fetch(`${API}${PORT}/data`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(data), // body data type must match "Content-Type" header
  });
};

const sendJSONData = () => {
  inspectSelectionIndex(memo.lastSelection);
  clearSelection();
  const ID = Date.now() + (Math.random() * 123).toFixed(0);
  const name = memo.data.title?.trim();
  const filename = name ? name.replaceAll(" ", "_") + "_" + memo.data.lvl : ID;
  memo.data.filename = filename;
  memo.data.id = ID;
  memo.elements = cy.json().elements;

  upload({
    version: VERSION,
    dir:'graph/',
    filename: filename,
    data: memo.data,
    elements: memo.elements,
  });
};

const normalize = (val, max, min) => (val - min) / (max - min);

const offsetElementsIndexes = (elements) => {
  const N = memo.nodeIndex;
  const E = memo.edgeIndex;
  const { nodes, edges } = elements;

  let maxNodeIndex = -Infinity;
  let maxEdgeIndex = -Infinity;

  const offsetNodes = nodes?.map((n) => {
    n.data.index += N;
    n.data.id = "n" + n.data.index;
    maxNodeIndex = Math.max(maxNodeIndex, n.data.index);
    return n;
  });

  const offsetEdges = edges?.map((e) => {
    const index = Number(e.data.id.substr(1)) + E;
    e.data.id = "e" + index;
    e.data.source = `n${Number(e.data.source.substr(1)) + N}`;
    e.data.target = `n${Number(e.data.target.substr(1)) + N}`;
    maxEdgeIndex = Math.max(maxEdgeIndex, index, E);
    return e;
  });

  incIndex(maxNodeIndex);
  memo.edgeIndex = Math.max(maxEdgeIndex, memo.edgeIndex) + 1;
  return { nodes: offsetNodes || [], edges: offsetEdges || [] };
};

const loadFile = () => {
  clearSelection();
  inspectSelectionIndex({ type: "not selected", id: "none" });
  const temp = document.createElement("button");
  temp.innerHTML = "add tree";
  // elements.loadedFileText.textContent = "tree ready to be added on stage";
  temp.id = "openJSONButt";
  elements.formData.parentNode.appendChild(temp);
  elements.fromJSONButt.style.display = "none";
  temp.addEventListener("click", (e) => {
    const reader = new FileReader();
    reader.onload = function (e) {
      const parsedJSON = JSON.parse(e.target.result);
      memo.elements = offsetElementsIndexes(parsedJSON.elements);
      memo.data = parsedJSON.data;
      elements.titleInput.value = parsedJSON.data.title;
      elements.levelInput.value = parsedJSON.data.lvl;

      seedGraph();
      // elements.loadedFileText.textContent = parsedJSON.filename + ".json";

      if (VERSION !== parsedJSON.version) {
        console.warn(
          `File version (${parsedJSON.version}) is older than current one ${VERSION}`
        );
      }
    };
    try {
      reader.readAsBinaryString(elements.formData.files[0]);
    } catch (err) {
    } finally {
      elements.fromJSONButt.innerHTML = "load";
      elements.formData.value = null;
      e.target.parentNode.removeChild(e.target);
      elements.fromJSONButt.style.display = "inline-block";
    }
  });
};
const test = () => {

//console.log('test')
// console.log(compile())
};
const getScroll = () => {
  if (window.pageYOffset !== undefined) {
      return {x:pageXOffset, y:pageYOffset};
  } else {
      var sx, sy, d = document,
          r = d.documentElement,
          b = d.body;
      sx = r.scrollLeft || b.scrollLeft || 0;
      sy = r.scrollTop || b.scrollTop || 0;
      return {x:sx, y:sy};
  }
}
export {getScroll, clearFileOptions, loadSelectedFile, createFileSelect, getJSONData, test, loadFile,compile, sendJSONData, clearTree, pruneTree };
