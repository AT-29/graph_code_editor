const VERSION = 1.0;
const memo = {
  lastSelection: { id: null },
  selectedPairs: [],
  mousePosition: { x: 0, y: 0 },
  timer:null,
  nodeIndex: 0,
  edgeIndex: 0,
  elements: {},
  data: {
    filename: "",
    id: "",
    lvl: "",
    title: "",
  },
};



const elements = {
  treeContainer: document.getElementById("tree"),
  selectedIndex: document.getElementById("selectedIndex"),
  toJSONButt: document.getElementById("toJSONButt"),
  fromJSONButt: document.getElementById("fromJSONButt"),
  formData: document.getElementById("formData"),
  loadedFileText: document.getElementById("loadedFileText"),
  nameInput: document.getElementById("nameInput"),
  clearButt: document.getElementById("clearButt"),

  // indexInput: document.getElementById("indexInput"),
  infoGuide: document.getElementById("infoGuide"),
  tooltip: document.getElementById("tooltip"),
  titleInput: document.getElementById("titleInput"),
  levelInput: document.getElementById("levelInput"),
  fileSelector:document.getElementById("fileSelector"),
  compileButt:document.getElementById("compileButt"),
  output: document.getElementById("output"),
  
};
export { memo, elements, VERSION };
