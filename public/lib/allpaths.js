
export const cytoscapeAllPaths =  function () {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      _ref$maxPaths = _ref.maxPaths,
      maxPaths = _ref$maxPaths === void 0 ? -1 : _ref$maxPaths,
      _ref$rootIds = _ref.rootIds,
      rootIds = _ref$rootIds === void 0 ? [] : _ref$rootIds;

  var eles = this;
  var cy = this.cy(); // 1. Find all root node

  var rootNodes = [];

  if (rootIds.length === 0) {
    rootNodes = eles.roots();
  } else {
    rootNodes = rootIds.map(function (id) {
      return cy.$id(id);
    }).filter(function (item) {
      return item.length > 0;
    });
  } // 2. Start with each root node and traverse all paths


  var allPaths = []; // [[node, edge, node, edge, ...], ...]

  rootNodes.forEach(function (rNode) {
    traversing(rNode, [rNode]);
  });

  function traversing(node, preNodes) {
    if (maxPaths >= 0 && allPaths.length >= maxPaths) return;

    if (!node) {
      // It's the ending node
      allPaths.push(preNodes);
      return;
    }

    var nextEles = getOutgoers(node);

    if (nextEles.length === 0) {
      // It's the ending node
      allPaths.push(preNodes);
    } else {
      nextEles.forEach(function (pairEles) {
        if (preNodes.find(function (item) {
          return item.id() === pairEles[1].id();
        })) {
          // Prevent circular dependence
          traversing(null, preNodes.concat(pairEles));
        } else {
          traversing(pairEles[1], preNodes.concat(pairEles));
        }
      });
    }
  }

  function getOutgoers(node) {
    var outgoers = node.outgoers();
    var nextEles = []; // [[node, edge], ...]

    var eles = []; // [node, edge]

    outgoers.forEach(function (oEle, idx) {
      if (idx % 2 == 0) {
        // even
        eles = [];
        eles.push(oEle);
      } else {
        // odd
        eles.push(oEle);
        nextEles.push(eles);
      }
    });
    return nextEles;
  }

  var allPathsCollection = [];
  allPaths.forEach(function (pathItem) {
    var pathCollection = cy.collection(pathItem);
    allPathsCollection.push(pathCollection);
  });
  return allPathsCollection; // chainability
};



