import { Code } from './main.js';
import { UI } from "./UI/dom_builder.js";
import { dragElement } from "./UI/drag_element.js";

export class Interface {
  #runes = {};
  #element;
  #nameSpace = "_";
  #editor;
  #output;
  #input;
  #documentation = new Map();
  #LineOffset = 4;
  changesStack = [];
  #tabs = new Map();
  code = {
    fullSource: "",
    JSandJSML: "",
    JSML_Tags: "",
    javascript: "",
    standatLib: `const ${this.nameSpace}=(()=>({pipe:(...fn)=>(x)=>fn.reduce((v,f)=>f(v),x),curry:(fn, ...val)=>(...nxt)=>(nxt.length) ? stdn.curry(fn, ...val, ...nxt) : fn(...val),typeOf:(inp)=>({toEqual:(type)=>{if(inp === undefined){throw new Error("incoming value is not defined")}else if(inp.constructor.name!==type.constructor.name)throw new Error("Expected "+ type.constructor.name +" but got "+ inp.constructor.name)}}),}))();\n`
  };

  width;
  height;
  isAppOpened = true;

  constructor(divEl, outputEl, options) {
    this.#editor = CodeMirror(divEl, options);
    this.#output = outputEl;
  }
  get nameSpace() {
    return this.#nameSpace;
  }
  get runes() {
    return this.#runes;
  }
  get input() {
    return this.#input;
  }
  set input(val) {
    this.#input = val;
  }

  get editor() {
    return this.#editor;
  }
  get tabs() {
    return this.#tabs;
  }
  get cursor() {
    return { ...this.editor.getCursor() };
  }

  get element() {
    return this.#element;
  }
  set element(val) {
    this.#element = val;
  }
  setSize = (x, y) => {
    this.editor.setSize(x, y);
    this.width = this.editor.getWrapperElement().clientWidth;
    this.height = this.editor.getWrapperElement().clientHeight;
  };
  setRunes(runes) {
    this.#runes = runes;
  }
  log(value, color) {
    this.#output.value = value;
    this.#output.style.color = color;
  }

  parse(source) {
    const runesRgx = Object.keys(this.runes).join("|");
    const RuneRgx = new RegExp(runesRgx, "g");
    return source.replace(RuneRgx, (matched) => {
      if (this.runes[matched]) {
        return this.runes[matched];
      } else {
        return matched;
      }
    });
  }
  //   open:function(file='app'){
  //      setTimeout(()=>{
  // const win = window.open(file+'.html',file.toUpperCase(),'toolbar=no,width=600,height=500,left=700,top=200,status=no,scrollbars=no,resize=no');
  // return setTimeout(()=>{
  //   win.document.write(this.raw()); this.resetSettings();
  // },this.buidDelay);
  //      },this.openDelay);
  // }

  openWindow(data, file = "app") {
    setTimeout(() => {
      const iframe = document.getElementById("app_window");
      //  const url = document.getElementById('url-input').value
      const app_window = iframe.contentWindow;
      //  if(url){
      //    iframe.src = url;
      //    iframe.src = iframe.src
      //   // iframe.contentWindow.location.reload();
      //  }else{
      app_window.location.reload();
      return setTimeout(() => {
        app_window.document.write(data);
        //app.resetSettings();
        //win.document.write(app.raw()); app.resetSettings();
      }, 100);
      //  }
    }, 0);
  }
  buildApp() {
    //const jsmlTagContent = htmlContent[1].split('</jsml>')[0];
    const htmlOutput = `<body></body><script src="editor/lib/tree/tree.js"></script><script>${
      this.code.standatLib
    }</script><script>
  const  ${
    this.nameSpace
  }Source = (()=>{\n${this.code.javascript.trim()}\n})\n ${
      this.nameSpace
    }Source(); \n</script>\n`;
    this.openWindow(htmlOutput);
  }

  catchErrors(callback) {
    try {
      callback();
    } catch (err) {
      const stack = err.stack.split("<anonymous>:");
      if (err.stack && stack[1]) {
        const offset = this.#LineOffset;
        const stackLen = stack.length;
        const errCoords = err.stack
          .split("mous>:")
          [stackLen - 2].split(")")[0]
          .split(":");
        const line = errCoords[0] - offset;
        const ch = errCoords[1];
        const ERROR_MSG =
          err.stack.split("Error:")[0] +
          "Error at line " +
          line +
          ": " +
          err.message;
        const errorEl = UI.create("button", {
          id: "error_line",
          innerHTML: " " + err.message,
        });
        this.editor.addWidget({ line, ch: Infinity }, errorEl, true);
        setTimeout(() => {
          errorEl.parentNode.removeChild(errorEl);
        }, 3000);

        this.editor.focus();
        this.log(ERROR_MSG, "#ff275d");
      } else {
        this.log(
          err.stack.split("Error:")[0] + "Error: " + err.message,
          "#ff275d"
        );
        console.error(err);
      }
    }
  }
  pipeOutput='';
  result = undefined;
  setTabs(path) {
    const fns = [];
    // console.log(path.path.select())
    this.#tabs = Object.values(path.data).reduce((map, x) => {
      if (x.text ) {
        fns.push(x.id);
        map.set(x.id, `const ${x.id}=(input)=>{
          let output = input;
          ${x.text};
          return output};\n`);
      } else {
        map.set(x.id, "");
      }
      return map;
    }, new Map());

this.pipeOutput = `return ${this.nameSpace}.pipe(${fns.join(',')})();`
  }
   serialize(obj){
   return JSON.stringify(obj, (k,v)=>{
      if(typeof v === "function")
          return 'fn()'
      return v;
  });
   } 
 // return ${val.output[0]}${val.output[1].join(',')}${val.output[2]};`);
  runCode() {
    this.catchErrors(() => {
      this.code.JSML_Tags = [...this.tabs.values()].join("") + '\n' + this.pipeOutput;
      this.code.javascript = this.code.JSML_Tags;
     
      // const replaced = this.parse(this.code.JSML_Tags);
      // this.code.javascript = replaced;
      const parsed = this.code.standatLib + this.code.javascript;
      this.code.JSandJSML = parsed;
      if(this.isAppOpened) setTimeout(()=>this.buildApp(),100);
      else{
        const result = new Function(
          `"use strict"; return ((()=>{ ${parsed}})())`
        )();
        this.result = this.serialize(result);
        this.log(this.result, "rgb(85, 226, 100)");
        return result;
      }
     
    });
  }

  openAppWindow() {
    const appWin = document.getElementById("sketchContainer");
    if (!this.isAppOpened) {
   
      // this.editor.display.input.textarea.style.width = '50%';

      this.editor.setSize(this.width - 750, this.height);
      appWin.style.display = "block";
      // document.getElementById('url-input').style.display = 'inline-block'
      // document.getElementById('windowMode').style.filter = 'grayscale(0)';
    } else {
   
      // document.getElementById('url-input').style.display = 'none'
      this.editor.setSize(this.width, this.height);
      // this.editor.display.input.textarea.style.width = '90%';

      appWin.style.display = "none";
      // document.getElementById('windowMode').style.filter = 'grayscale(100)';
    }
    this.isAppOpened = !this.isAppOpened;
  }
  changeTheme(theme) {
    this.editor.setOption("theme", theme);
    location.hash = "#" + theme;
  }
  // getSnippetsFromExternal(external) {
  //   if (external) {
  //     const link = `https://raw.githubusercontent.com/AnthonyTonev/hyper_code_snippets/master/${external}.json`;
  //     fetch(link)
  //       .then((response) => response.json())
  //       .then((data) => {
  //         this.editor.setValue(
  //           this.editor.getValue() + '\n' + data.code + '\n'
  //         );
  //         this.editor.scrollIntoView({ line: this.editor.lastLine(), char: 0 });
  //         this.log(data.title + ' imported!', '#ffffff');
  //         if (data.description) {
  //           data.description.map((item) => {
  //             this.#documentation.set(item.id, {
  //               description: item.description,
  //               example: item.example,
  //             });
  //           });
  //         }
  //       })
  //       .catch((err) => {
  //         this.log(
  //           'Snippet does not exist or it has bad JSON format.',
  //           '#f3e110'
  //         );
  //       });
  //     return;
  //   }
  // }
  addLegend() {
    const legend = `/*
  ${this.nameSpace}.pipe(...fn)
  ${this.nameSpace}.curry(...fn)
  ${this.nameSpace}.typeOf(output),toEqual(Boolean());
${Object.entries(this.#runes).reduce((acc, item) => {
  acc += item[0] + "        " + item[1] + "\n";
  return acc;
}, "")}

*/
    
    `;
    this.editor.setValue(legend + "\n" + this.editor.getValue());
    // this.openTabs(legend,67,40);
    // this.editor.setValue(legend+ '\n'+ this.editor.getValue())
  }

  undock() {
    const div = UI.create("div", document.body, {
      style: "border:1px solid #99b2ff;padding:1%;",
    });
    this.element = div;
    const dragg = UI.create("button", div, {
      class: "ui",
      style:
        "cursor:pointer;font-size:20px;width:40px;height:40px;text-align:center;margin-left:2%;margin-top:-1%",
        textContent: "❖",
      drag: "true",
    });

    div.appendChild(document.getElementById("app_container"));
    dragg.addEventListener("click", () => {
      const drag = dragg.getAttribute("drag");
      if (drag === "false") {
        dragg.setAttribute("drag", "true");
        dragg.textContent = "❖";
      } else {
        dragg.setAttribute("drag", "false");
        dragg.textContent = "⁜";
      }
    });

    dragElement(div, dragg);
    div.style.position = "absolute";
    div.style.top = "3%";
    div.style.left = "3%";
    // node.setAttribute("cm-ignore-events", "true");
    // this.editor.addWidget(this.cursor, div, true);
    
  }
}
  



