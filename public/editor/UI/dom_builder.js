const UI = {
  parent: document.body,
  setParent: function (handler) {
    this.parent = handler;
  },
  create: function (type = '', parent, params) {
    let parentEl;
    if (parent instanceof HTMLElement) {
      parentEl = parent;
    } else {
      parentEl = this.parent;
      params = parent;
    }
    switch (type) {
      default:
        {
          const element = document.createElement(type);
          parentEl?.appendChild(element);
          if (params) {
            for (const i in params) {
              if (i != 'textContent' && i != 'innerHTML') {
                element.setAttribute(i, params[i]);
              } else {
                element[i] = params[i];
              }
            }
          }
          return element;
        }
        break;
    }
  }
};

export { UI };
