import { UI } from './UI/dom_builder.js';
import { dragElement } from "./UI/drag_element.js";
import { Interface } from './editor.js';

const clearNodesByClass= (className='log_box') => {
  console.clear();
  [...document.getElementsByClassName(className)].forEach((item) =>
    item.parentNode.removeChild(item)
  );
};

  UI.setParent(document.getElementById('app_container'));  
  const toolbar = UI.create('div')
  // UI.create('button', toolbar,{
  //   textContent: '▷',
  //   class: 'ui',
  //   style: 'width:55px;heigth:25px; margin-right:1%',
  // }).addEventListener('click', () => Code.runCode());
  // UI.create('button',toolbar, {
  //   id:'windowMode',
  //   innerHTML: '&#9639;',
  //   class: 'ui',
  //   style: 'width:55px;heigth:25px; margin-right:1%',
  // }).addEventListener('click', () => Code.openAppWindow());

  UI.create('input',toolbar, {
    id: 'console',
    disabled: true,
    class: 'ui'
  });




  const Code = new Interface(
    document.getElementById('app_container'),
    document.getElementById('console'),
    {
      value: '',
      mode: 'javascript',
      tabSize: 5,
      theme: 'night',
      lineNumbers: true,
      styleActiveLine: true,
      matchBrackets: true,
      onGutterClick: CodeMirror.newFoldFunction(CodeMirror.braceRangeFinder),
      extraKeys: {},
      gutters: ['CodeMirror-linenumbers', 'CodeMirror-foldgutter'],
      lint: false,
      foldGutter: true,
    }
  );
  Code.setSize(600, 500);

  // document.addEventListener('keydown', (e) => {
  //   if (e.key.toLowerCase() === 'q' && e.ctrlKey) Code.runCode();
  // });

  UI.create('button', {
    textContent: 'terminal',
    class: 'ui',
  }).addEventListener('click', (e) =>{
    if(e.target.textContent==='window'){
      e.target.textContent = 'terminal';
      Code.openAppWindow();
    }else{
      e.target.textContent = 'window';
      Code.openAppWindow();
    }

  } );


  // UI.create('button', {
  //   textContent: 'build',
  //   class: 'ui',
  // }).addEventListener('click', () =>{
  //   Code.buildApp();
  // } );


  UI.create('button', {
    textContent: 'js',
    class: 'ui',
  }).addEventListener('click', (e) =>{
    if(e.target.textContent==='js'){
      e.target.textContent = 'JSML';
      Code.editor.setValue(Code.code.JSandJSML);
    }else{
      e.target.textContent = 'js';
      Code.editor.setValue(Code.code.fullSource);
    }

  } );


  UI.create('button', {
    innerHTML: '{...}',
    class: 'ui',
  }).addEventListener('click', (e) =>{
    if(e.target.innerHTML==='{...}'){
      e.target.innerHTML = '{&crarr;}';
      CodeMirror.commands.foldAll(Code.editor);
    }else{
      CodeMirror.commands.unfoldAll(Code.editor);
      e.target.innerHTML = '{...}';
 
    }

  } );

  UI.create('button', {
    textContent: 'legend',
    class: 'ui',
  }).addEventListener('click', () =>{
    Code.addLegend();
  } );

  //thisi s just because i'm lazy to make it dynamic;

Code.undock();
const appWindow = document.getElementById("sketchContainer")

dragElement(document.getElementById('fileSelector') );
dragElement(document.getElementById('infoPanel'), (()=>{
  const parent = document.getElementById('infoPanel')
  const dragg = UI.create("button", parent, {
    class: "ui",
    style:
      "cursor:pointer;font-size:20px;width:40px;height:40px;text-align:center;position:absolute;top:10%",
    textContent: "❖",
    drag: "true",
  });


  dragg.addEventListener("click", () => {
    const drag = dragg.getAttribute("drag");
    if (drag === "false") {
      dragg.setAttribute("drag", "true");
      dragg.textContent = "❖";
    } else {
      dragg.setAttribute("drag", "false");
      dragg.textContent = "⁜";
    }
  });

  dragElement(parent, dragg);

  return dragg;
})()

);

dragElement(appWindow);
export {Code}